/**
 * PrimitivaComunicacion.java
 * Guillermo Bambó Pueyo
 * 
 */
package control;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Primitiva de comunicación cliente-servidor
 *
 */
public enum PrimitivaComunicacion {
    REALIZAR_MOVIMIENTO("do_movement"),
    CONECTAR("online"),
    DESCONECTAR("offline"),
    PEDIR_JUGADORES_ONLINE("get_players_online"),
    JUGAR_CON("play_with"),
    NUEVA_PARTIDA("new_game"),
    ABANDONAR_PARTIDA("surrender"),
    TEST("test"),
    FIN("."),
    OK("ok"),
    NOK("nok");

    private String simbolo;
    private static final Pattern expresionRegular
            = Pattern.compile(REALIZAR_MOVIMIENTO.toString() + "|"
                    + CONECTAR.toString() + "|"
                    + DESCONECTAR.toString() + "|"
                    + PEDIR_JUGADORES_ONLINE.toString() + "|"
                    + JUGAR_CON.toString() + "|"
                    + NUEVA_PARTIDA.toString() + "|"
                    + TEST.toString() + "|" 
                    + ABANDONAR_PARTIDA.toString() + "|"                    
                    + FIN.toString() + "|"
                    + OK.toString() + "|"
                    + NOK.toString());

    /**
     * Construye una primitiva
     *
     */
    PrimitivaComunicacion(String simbolo) {
        this.simbolo = simbolo;
    }

    /**
     * Devuelve una nueva primitiva leída de un scanner
     *
     */
    public static PrimitivaComunicacion nueva(Scanner scanner) throws InputMismatchException {
        String s = scanner.next(expresionRegular);
        if (s.equals(CONECTAR.toString())) {
        return CONECTAR;    
        }
        if (s.equals(REALIZAR_MOVIMIENTO.toString())) {
            return REALIZAR_MOVIMIENTO;
        } else if (s.equals(DESCONECTAR.toString())) {
            return DESCONECTAR;
        } else if (s.equals(PEDIR_JUGADORES_ONLINE.toString())) {
            return PEDIR_JUGADORES_ONLINE;
        } else if (s.equals(JUGAR_CON.toString())) {
            return JUGAR_CON;
        } else if (s.equals(NUEVA_PARTIDA.toString())) {
            return NUEVA_PARTIDA;    
        } else if (s.equals(ABANDONAR_PARTIDA.toString())) {
            return ABANDONAR_PARTIDA;
        } else if (s.equals(FIN.toString())) {
            return FIN;
        } else if (s.equals(OK.toString())) {
            return OK;
        } else {
            return NOK;
        }
    }

    /**
     * toString
     *
     */
    @Override
    public String toString() {
        return simbolo;
    }
}