/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import modelo.Ficha;
import modelo.Movimiento;
import modelo.Posicion;

/**
 *
 * @author Guille
 */
public class ServidorDama implements Runnable {
    public static String ERROR_PLAYER_CONNECTION = "Error!! Closed user connection";
    private static String FORMATO_FECHA_CONEXION = "kk:mm:ss EEE d MMM yy";

    private ServidorDamas servidorDamas;
    private Socket socket;
    private BufferedReader entrada;
    private PrintWriter salida;
//    private ConexionBD conexionBD;
    
    
    public ServidorDama(ServidorDamas servidorDamas, Socket socket)
        throws IOException {
        this.servidorDamas = servidorDamas;
        this.socket = socket;
        entrada = new BufferedReader(
            new InputStreamReader(socket.getInputStream()));
        // Autoflush!!
        salida = new PrintWriter(new BufferedWriter(
            new OutputStreamWriter(socket.getOutputStream())), true);
    }

    @Override
    public void run() {
        try {
            PrimitivaComunicacion solicitud = PrimitivaComunicacion.nueva(
                new Scanner(new StringReader(entrada.readLine())));
            switch(solicitud){
                case CONECTAR:
                    conectarJugador();
                break;
                case REALIZAR_MOVIMIENTO: 
                    realizarMovimiento();
                break;             
                case ABANDONAR_PARTIDA:
                    finalizarPartida();
                break;
                case DESCONECTAR:
                    desconectarJugador();
                break;
                case PEDIR_JUGADORES_ONLINE:
                    pedirJugadoresOnline();  
                break;
                case JUGAR_CON:
                    jugarCon();
                break;              
            }
        } catch (InputMismatchException e1) {
            if (ServidorDamas.esModoDebug()) {
            System.out.println("Request wrong from " + socket.getInetAddress() + 
                           " at "  /*+ devuelveFechaHoy()*/);
            }    
        } catch (Exception e2) {
            System.out.println("ERROR_PLAYER_CONNECTION" + ": " + e2.toString());
            if (ServidorDamas.esModoDebug()) {
                 e2.printStackTrace();
            }         
        }
    }
    
    private void realizarMovimiento(){
        try{
          
        String solicitante = entrada.readLine();
        String solicitado = entrada.readLine();   
        String movimiento = entrada.readLine();
        
        Movimiento mov = new Movimiento(movimiento);
        Ficha ficha = mov.devuelveFicha();
        Posicion pos = mov.devuelvePosicion();

        if (ServidorDamas.esModoDebug()) {
          System.out.println("Request: " + 
            PrimitivaComunicacion.REALIZAR_MOVIMIENTO.toString() + " " +
             solicitante + " " +
             solicitado + " " + ficha + " " + pos +
             " at " + devuelveFechaHoy());
        }
        
        // enviamos a solicitado
        ConexionPushJugadorOnline conexionJugadorSolicitado = 
           servidorDamas.devuelveConexionJugadorOnline(solicitado);

        // si jugador solicitado no está online lo comunicamos al solicitante
        if (conexionJugadorSolicitado == null) {
           salida.println(PrimitivaComunicacion.NOK.toString());   
        } 
         // enviamos jugada a solicitado
        else {
           salida.println(PrimitivaComunicacion.OK.toString());   
           salida.flush();
           conexionJugadorSolicitado.enviarSolicitud(PrimitivaComunicacion.REALIZAR_MOVIMIENTO,
             ficha + "\n" + pos);    
        }
        entrada.close();
        salida.close();
        socket.close();  
        }catch(Exception e) {
            
        }
    }
  /**
   *  Devuelve fecha de hoy 
   * 
   */      
  private String devuelveFechaHoy() {
    return new SimpleDateFormat(FORMATO_FECHA_CONEXION, 
                                Locale.getDefault()).format(new Date());
  }
  
  private void conectarJugador() throws IOException, InterruptedException {
    try {  
        String solicitante = entrada.readLine();
        
        if (ServidorDamas.esModoDebug()) {
          System.out.println("Request: " + 
            PrimitivaComunicacion.CONECTAR.toString() + " " +
            solicitante + " at " + devuelveFechaHoy());
        }
        // creamos conexión push por long polling    
        CountDownLatch cierreConexion = new CountDownLatch(1);
        servidorDamas.nuevaConexionPushJugadorOnline(
            new ConexionPushJugadorOnline(solicitante, socket, cierreConexion));      
        // y esperamos hasta cierre conexión 
        cierreConexion.await();
    } catch(Exception e) {
      if (ServidorDamas.esModoDebug()) {
          e.printStackTrace();
      } 
    }
  }
    /**
    *  Atiende solicitud jugador desconectado
    * 
    */    
   private void desconectarJugador() throws IOException {
        String solicitante = entrada.readLine();

        if (ServidorDamas.esModoDebug()) {
          System.out.println("Request: " + 
            PrimitivaComunicacion.DESCONECTAR.toString() + " " +
            solicitante + " at " + devuelveFechaHoy());
        }   

        servidorDamas.cerrarConexionJugadorOnline(solicitante);

        entrada.close();
        salida.close();
        socket.close();
   }     
  /**
   *  Atiende solicitud pedir jugadores online
   * 
   */    
  private void pedirJugadoresOnline() throws IOException {  
    String solicitante = entrada.readLine();
    
    if (ServidorDamas.esModoDebug()) {
      System.out.println("Request: " + 
        PrimitivaComunicacion.PEDIR_JUGADORES_ONLINE.toString() + " " +
        solicitante + " at " + devuelveFechaHoy());
    }         
    // enviamos los jugadores online
    salida.println(PrimitivaComunicacion.PEDIR_JUGADORES_ONLINE.toString());  
    for(String jugador : servidorDamas.devuelveNombreJugadoresOnline()) {  
      if ( ! jugador.equals(solicitante)) {
        salida.println(jugador);
      }
    }
    salida.println(PrimitivaComunicacion.FIN);
    entrada.close();
    salida.close();
    socket.close();  
  }  
  
  
    /**
     *  Atiende solicitud jugar con 
     * 
     */ 
     private void jugarCon() throws Exception {
        String turnoSolicitante;
        String turnoSolicitado;
        PrimitivaComunicacion resultado = null;

         String solicitante = entrada.readLine();
         String solicitado = entrada.readLine();

         if (ServidorDamas.esModoDebug()) {
             System.out.println("Request: "
                     + PrimitivaComunicacion.JUGAR_CON.toString() + " "
                     + solicitante + " "
                     + solicitado + " at " + devuelveFechaHoy());
         }

         ConexionPushJugadorOnline conexionJugadorSolicitado
                 = servidorDamas.devuelveConexionJugadorOnline(solicitado);

         // si jugador solicitado no está online lo comunicamos al solicitante
         if (conexionJugadorSolicitado == null) {
             salida.println(PrimitivaComunicacion.NOK.toString());
         } // invitamos a jugar
         else {
             resultado = conexionJugadorSolicitado.enviarSolicitud(
                     PrimitivaComunicacion.JUGAR_CON, solicitante);

             // Si no quiere lo comunicamos a solicitante
             if (resultado == PrimitivaComunicacion.NOK) {
                 salida.println(PrimitivaComunicacion.NOK.toString());
             } // si quiere jugar establecemos turno aleatorio y comunicamos nueva 
             // partida online
             else {
                 boolean turno = new Random().nextBoolean();
                 if (turno == false) {
                     turnoSolicitante = "Blanco";
                     turnoSolicitado = "Negro";
                 } else {
                     turnoSolicitante = "Negro";
                     turnoSolicitado = "Blanco";
                 }

                 System.out.println("turno solicitante " + turnoSolicitante);
                 salida.println(PrimitivaComunicacion.NUEVA_PARTIDA);
                 salida.println(turnoSolicitante);
                 salida.println(solicitado);
                 salida.println(PrimitivaComunicacion.FIN);

                 conexionJugadorSolicitado.enviarSolicitud(PrimitivaComunicacion.NUEVA_PARTIDA,
                         turnoSolicitado.toString() + "\n" + solicitante);
             }
         }

         entrada.close();
         salida.close();
         socket.close();
     }
    /**
     *  Atiende solicitud finalizar partida
     * 
     */ 
     private void finalizarPartida() throws Exception {    
       String solicitante = entrada.readLine();  
       String solicitado = entrada.readLine(); 

       if (ServidorDamas.esModoDebug()) {
         System.out.println("Request: " + 
           PrimitivaComunicacion.ABANDONAR_PARTIDA.toString() + " " +
           solicitante + " " +
           solicitado + " at " + devuelveFechaHoy());
       }    

       salida.println(PrimitivaComunicacion.OK.toString());

       ConexionPushJugadorOnline conexionJugadorSolicitado = 
           servidorDamas.devuelveConexionJugadorOnline(solicitado);

       if (conexionJugadorSolicitado != null) {
         conexionJugadorSolicitado.enviarSolicitud(
           PrimitivaComunicacion.ABANDONAR_PARTIDA, solicitante);
       }

       entrada.close();
       salida.close();
       socket.close();
     }
}
