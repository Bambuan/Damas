/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Guille
 */
public class ServidorDamas {
    private ServidorDamas servidorDamas;
    public static String VERSION = "Damas Server 1.0";
    private static String ARG_DEBUG = "-d";

    private static String FICHERO_CONFIG_WRONG
            = "Config file is wrong. Set default values";
    private static String WAITING_USER_REQUEST
            = "Waiting for user requests...";
    private static String ERROR_SERVER_RUNNING = "Error: Server running in ";
    private static String ERROR_CREATING_USER_CONNECTION
            = "Failed to create user connection";

    private static int TIEMPO_COMPROBACION_CONEXIONES = 10 * 1000;
    
    private Map<String, ConexionPushJugadorOnline> conexionesJugadoresOnline = 
          new ConcurrentHashMap<>();
    
    private static boolean modoDebug = false;

    /**
     * Configuración
     */
    private Properties propiedades;
    private static final String FICHERO_CONFIG = "config.properties";

    private static final String NUM_THREADS = "threadsNumber";
    private int numThreads = 16;
    private static final String PUERTO_SERVIDOR = "serverPort";
    private int puertoServidor = 15000;

    /**
     * Construye el servidor de juegos
     *
     */
    ServidorDamas() {
        servidorDamas = this;
        leerConfiguracion();
        ejecutar();
    }
    /**
     * Lee configuración
     *
     */
    private void leerConfiguracion() {
        try {
            propiedades = new Properties();
            propiedades.load(new FileInputStream(FICHERO_CONFIG));

            numThreads = Integer.parseInt(propiedades.getProperty(NUM_THREADS));
            puertoServidor = Integer.parseInt(propiedades.getProperty(PUERTO_SERVIDOR));
        } catch (Exception e) {
            System.out.println(FICHERO_CONFIG_WRONG);
            System.out.println(NUM_THREADS + " = " + numThreads);
            System.out.println(PUERTO_SERVIDOR + " = " + puertoServidor);
            if (esModoDebug()) {
                e.printStackTrace();
            }
        }
    }
    
    
    public static boolean esModoDebug() {
        return modoDebug;
    }

    /**
     * Ejecuta servidor juegos
     *
     */
    private void ejecutar() {
        System.out.println(VERSION);
        try {
            ExecutorService poolThreads = Executors.newFixedThreadPool(numThreads);
            comprobarConexionesAbiertas();
            
            ServerSocket ss = new ServerSocket(puertoServidor);
            while (true) {
                System.out.println(WAITING_USER_REQUEST);
                Socket socket = ss.accept();
                poolThreads.execute(new ServidorDama(this, socket));
            }
        } catch (BindException e) {
            System.out.println(ERROR_SERVER_RUNNING + puertoServidor);
        } catch (IOException e) {
            System.out.println(ERROR_CREATING_USER_CONNECTION);
            if (esModoDebug()) {
                e.printStackTrace();
            }
        }
    }
    /**
    *  Nuevo jugador online 
    * 
    */   
    public ConexionPushJugadorOnline nuevaConexionPushJugadorOnline(
                ConexionPushJugadorOnline conexion) {
        return conexionesJugadoresOnline.put(conexion.devuelveNombre(), conexion);  
    }
    /**
     *  Quita jugador online
     * 
     */   
    public void cerrarConexionJugadorOnline(String nombre) throws IOException {
        conexionesJugadoresOnline.get(nombre).cerrar();
        conexionesJugadoresOnline.remove(nombre); 
    }  

    /**
     *  Devuelve jugador online
     * 
     */   
    public ConexionPushJugadorOnline devuelveConexionJugadorOnline(String nombre) {
        return conexionesJugadoresOnline.get(nombre);
    }    
    /**
     *  Devuelve nombres jugadores online
     * 
     */    
    public String[] devuelveNombreJugadoresOnline() {
        return conexionesJugadoresOnline.keySet().toArray(
            new String[conexionesJugadoresOnline.size()]);
    }
  
  /**
   * Comprueba conexiones abiertas
   *  
   */
  private void comprobarConexionesAbiertas() {  
    new Timer().scheduleAtFixedRate(new TimerTask() {
      @Override
      public void run() {
        for(ConexionPushJugadorOnline conexion : 
            conexionesJugadoresOnline.values()) {
          try {
            PrimitivaComunicacion respuesta = conexion.enviarSolicitud(
                   PrimitivaComunicacion.TEST, TIEMPO_COMPROBACION_CONEXIONES);
            
            if (respuesta == PrimitivaComunicacion.NOK) {
              new Exception();
            }
          } catch (Exception e1) {
             System.out.println(ServidorDama.ERROR_PLAYER_CONNECTION);
             conexionesJugadoresOnline.remove(conexion.devuelveNombre());
             try {
               conexion.cerrar();
             } catch (IOException e2) {
               // No hacemos nada, ya hemos cerrado conexión 
             } 
             if (ServidorDamas.esModoDebug()) {
               e1.printStackTrace();
             } 
          }
        }
      }          
    }, TIEMPO_COMPROBACION_CONEXIONES, TIEMPO_COMPROBACION_CONEXIONES);              
  }
      
    
    /**
     * Procesa argumentos de main
     *
     */
    private static void procesarArgsMain(String[] args) {
        List<String> argumentos = new ArrayList<String>(Arrays.asList(args));

        if (argumentos.contains(ARG_DEBUG)) {
            modoDebug = true;
        }
    }

    /**
     * Método main
     *
     */
    public static void main(String args[]) {
        procesarArgsMain(args);

        new ServidorDamas();
    }
}
