/*
 * Posicion.java
 * Guillermo Bambó Pueyo
 * 
 */
package modelo;

import java.io.PrintWriter;

/**
 *  Posicion del tablero
 * 
 */
public class Posicion {
    private int fila;
    private int columna;
    
    /**
     *  Crea una Posicion del tablero
     * 
     */
    public Posicion(int fila, int columna) {
        this.fila = fila;
        this.columna = columna;
    }
    
    /**
     *  Crea una posicion de un fichero
     * 
     */
    public Posicion(String linea) {
        String tokens[] = linea.split("'");
        
        this.fila = Integer.parseInt(tokens[0]);
        this.columna = Integer.parseInt(tokens[1]);                
    }
    
    /**
     *  Devuelve la fila
     * 
     */
    public int devuelveFila() {
        return fila;    
    }
    
    /**
     *  Devuelve la columna
     * 
     */
    public int devuelveColumna() {
        return columna;    
    }
    
    /**
     * Guarda posicion en un fichero de texto
     *
     */
    public void guardar(PrintWriter pw) {
        pw.print(this.fila + "'" + this.columna);    
    }
    
    /**
     * equals
     *
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Posicion)) {
            return false;
        }
        Posicion tmp = (Posicion) obj;
        return (fila == tmp.fila) && (columna == tmp.columna);
    }
    
    /**
     * toString
     *
     */
    @Override
    public String toString() {
        return (this.fila + "'" + this.columna);
    }
}
