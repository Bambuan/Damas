/*
 * Ficha.java
 * Guillermo Bambó Pueyo
 * 
 */
package modelo;

import java.io.PrintWriter;

/**
 *  Ficha de las damas
 * 
 */
public class Ficha {
    private int id;
    private String color;
    private Posicion posicion; //La posicion (-1,-1) indica que la ficha esta muerta
    private boolean esReina;
    
    /**
     *  Crea una Ficha
     * 
     */
    public Ficha(int id, String color, Posicion posicion, boolean esReina) {
        this.id = id;
        this.color = color;
        this.posicion = posicion;
        this.esReina = esReina;
    }   
    
    /**
     *  Crea una Ficha vacia
     * 
     */
    public Ficha() {
        this.id = -1;
        this.color = null;
        this.posicion = new Posicion(-1, -1);
        this.esReina = false;
    }
    
    /**
     *  Crea una ficha de un fichero
     * 
     */
    public Ficha(String linea) {
        String tokens[] = linea.split(",");
        
        this.id = Integer.parseInt(tokens[0]);
        this.color = tokens[1];        
        this.posicion = new Posicion(tokens[2]);       
        this.esReina = Boolean.valueOf(tokens[3]);
    }
    
    /**
     *  Devuelve el id de la ficha
     * 
     */
    public int devuelveId() {
        return id;    
    }
    
    /**
     *  Devuelve el color de la ficha
     * 
     */
    public String devuelveColor() {
        return color;    
    }
    
    /**
     *  Establece la posicion de la ficha
     * 
     */
    public void establecerPosicion(Posicion posicion) {
        this.posicion = posicion;
    }
    
    /**
     *  Devuelve la posicion de la ficha
     * 
     */
    public Posicion devuelvePosicion() {
        return posicion;    
    }
    
    /**
     *  Devuelve si la ficha es reina o no
     * 
     */
    public boolean esReina() {
        return esReina;    
    }
    
    /**
     *  Convierte a la ficha en reina
     * 
     */
    public void hacerReina() {
        this.esReina = true;    
    }
    
    /**
     * Guarda fichao en un fichero de texto
     *
     */
    public void guardar(PrintWriter pw) {
        pw.print(this.id + "," + this.color + "," + this.devuelvePosicion().toString()
            + "," + this.esReina());    
    }
    
    /**
     * equals
     *
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Ficha)) {
            return false;
        }
        Ficha tmp = (Ficha) obj;
        return (id == tmp.id) && (color.equals(tmp.color)) 
                && (posicion.equals(tmp.posicion)) && (esReina == tmp.esReina);
    }
    
    /**
     * toString
     *
     */
    @Override
    public String toString() {
        return (this.id + "," + this.color + "," + this.devuelvePosicion().toString()
            + "," + this.esReina());
    }
}
