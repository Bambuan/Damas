/*
 * Movimiento.java
 * Guillermo Bambó Pueyo
 * 
 */
package modelo;

/**
 *  Jugada de las damas
 * 
 */
public class Movimiento {
    private Ficha ficha;
    private Posicion posicion;
    
    public Movimiento(Ficha ficha, Posicion posicion) {
        this.ficha = ficha;
        this.posicion = posicion;
    }
    
    /**
     *  Crea una movimiento de un string
     * 
     */
    public Movimiento(String linea) {
        String tokens[] = linea.split("_");
        
        this.ficha = new Ficha(tokens[0]);  
        this.posicion = new Posicion(tokens[1]);       
    }
    
    public Ficha devuelveFicha() {
        return ficha;
    }
    
    public Posicion devuelvePosicion() {
        return posicion;
    }
    
    /**
     * toString
     *
     */
    @Override
    public String toString() {
        return (this.ficha.toString() + "_" + this.posicion.toString());
    }
}
