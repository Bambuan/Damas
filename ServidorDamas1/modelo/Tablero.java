/*
 * Guillermo Bambó Pueyo
 * 
 */
package modelo;

import java.io.PrintWriter;

/**
 *  Tablero de las damas
 * 
 */
public class Tablero {
    private static final int MAX_FICHAS = 24;
    private static final int DIM = 8;
    
    private Ficha fichas[] = new Ficha[MAX_FICHAS];
    
    /**
     *  Construye tablero de las damas
     * 
     */
    public Tablero() {

    }
    
    /**
     *  Construye tablero de las damas de fichero
     * 
     */
    public Tablero(String linea) {
        String tokens[] = linea.split("_");
        
        for (int i = 0; i < fichas.length; i++) {
            this.fichas[i] = new Ficha(tokens[i]);           
        }
    }
    
    /**
     * Elimina todas las fichas del tablero
     *
     */
    public void vaciar() {
        this.fichas = null;
    }
    
    /**
     * Coloca las fichas en el tablero en disposicion
     * para empezar una partida
     */
    public void colocarFichas() {
        int x = 0;
        //colocar fichas blancas
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < DIM; j++) {
                //Solo hay fichas en las posiciones pares (cuadros negros)
                if ((i + j) % 2 == 0) {
                    Posicion posicion = new Posicion(i, j);
                    Ficha ficha = new Ficha(x, "Blanco", posicion, false);
                    fichas[x] = ficha;
                    x++;
                }                
            }
        }
        
        //colocar fichas negras
        for (int i = DIM - 1; i > DIM - 4; i--) {
            for (int j = DIM - 1; j > DIM - 9; j--) {
                if ((i + j) % 2 == 0) {
                    Posicion posicion = new Posicion(i, j);
                    Ficha ficha = new Ficha(x, "Negro", posicion, false);
                    fichas[x] = ficha;
                    x++;
                }
            }
        }
    } 
    
    /**
     * Devuelve el numero de fichas del tablero
     *
     */
    public int devuelveNumFichas() {
        return this.fichas.length;
    }
    
    /**
     * Devuelve una ficha del tablero
     *
     */
    public Ficha devuelveFicha(int id) {
        Ficha ficha;
        for (int i = 0; i < fichas.length; i++) {
            if (fichas[i].devuelveId() == id) {
                ficha = fichas[i];
                return ficha;
            }
        }
        return null;
    }
    
    /**
     * Devuelve todas las fichas del tablero
     *
     */
    public Ficha[] devuelveFichas() {
        return this.fichas;
    }
    
    /**
     * Devuelve false en caso de que la posicion este fuera del tablero
     * 
     */
    public boolean posicionDentroTablero (Posicion posicion) {
        if (posicion.devuelveFila() < DIM && posicion.devuelveFila() >= 0 && 
           posicion.devuelveColumna() < DIM && posicion.devuelveColumna() >= 0 ) {
            return true;
        }
        return false;
    }
    
    /**
     * Devuelve si una posicion del tablero esta ocupada
     *
     */
    public boolean posicionOcupada(Posicion posicion) {               
        for (int i = 0; i < fichas.length; i++) {
            Posicion pos = fichas[i].devuelvePosicion();
            if (/*fichas[i].devuelvePosicion()*/pos.equals(posicion)) {
                return true;
            }
        }        
        return false;
    }
    
    /**
     * Devuelve si una posicion esta contigua a la ficha y valida para moverse
     *
     */
    public boolean posicionContiguaParaMover(Ficha ficha, Posicion posicion) {
        int filaFicha = ficha.devuelvePosicion().devuelveFila();
        int columnaFicha = ficha.devuelvePosicion().devuelveColumna();
        int filaPosicion = posicion.devuelveFila();
        int columnaPosicion = posicion.devuelveColumna();
        if (!ficha.esReina()) {
            if (ficha.devuelveColor().equals("Blanco")) {
                if((filaFicha + 1 == filaPosicion &&
                        columnaFicha - 1 == columnaPosicion) ||
                        (filaFicha + 1 == filaPosicion &&
                        columnaFicha + 1 == columnaPosicion)) {
                    return true;
                }else{
                    return false;
                }
            }else{
                if((filaFicha - 1 == filaPosicion &&
                        columnaFicha - 1 == columnaPosicion) ||
                        (filaFicha - 1 == filaPosicion &&
                        columnaFicha + 1 == columnaPosicion)) {
                    return true;
                }else{
                    return false;
                }
            }
        //Si la ficha es reina
        }else{
            if((filaFicha + 1 == filaPosicion &&
                        columnaFicha - 1 == columnaPosicion) ||
                        (filaFicha + 1 == filaPosicion &&
                        columnaFicha + 1 == columnaPosicion) ||
                        (filaFicha - 1 == filaPosicion &&
                        columnaFicha - 1 == columnaPosicion) ||
                        (filaFicha - 1 == filaPosicion &&
                        columnaFicha + 1 == columnaPosicion)) {
                    return true;
                }else{
                    return false;
                }
        }
    }
    
    /**
     * Cambia la posición de una ficha en el tablero
     *
     */
    public void moverFicha(Ficha ficha, Posicion nuevaPosicion) {
        for (int i = 0; i < fichas.length; i++) {
            if (fichas[i].devuelveId() == ficha.devuelveId()) {
                fichas[i].establecerPosicion(nuevaPosicion);      
            }
        }
    }
    
    /**
     * Saca la ficha del tablero (muerta), posicion(-1,-1)
     *
     */
    public void comerFicha(Ficha ficha) {
        for (int i = 0; i < fichas.length; i++) {
            if (fichas[i].devuelveId() == ficha.devuelveId()) {
                fichas[i].establecerPosicion(new Posicion(-1, -1));
            }
        }
    }
    
    /**
     * Hace una ficha reina
     *
     */
    public void hacerReina(Ficha ficha) {
        for (int i = 0; i < fichas.length; i++) {
            if (fichas[i].devuelveId() == ficha.devuelveId()) {
                fichas[i].hacerReina();
            }
        }
    }
    
    /**
     * Devuelve true si la ficha puede convertirse en reina
     *
     */
    public boolean puedeSerReina(Ficha ficha) {
        if (ficha.devuelveColor().equals("Blanco")) {
            if (ficha.devuelvePosicion().devuelveFila() == 7) {
                return true;
            }
        }else if (ficha.devuelveColor().equals("Negro")){
            if (ficha.devuelvePosicion().devuelveFila() == 0) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Devuelve true si la ficha esta muerta
     *
     */
    public boolean estaMuerta(Ficha ficha) {
        Posicion posicion = new Posicion(-1, -1);
        if (ficha.devuelvePosicion().equals(posicion)) {
            return true;
        }
        return false;
    }
    
    /**
     * Devuelve true si hay victoria en el tablero
     *
     */
    public boolean hayVictoria(String turno) {
        int contadorVictoria = 0;
        if (turno == "Blanco") {
            for (int i = 0; i < this.fichas.length; i++) {
                if (this.fichas[i].devuelveColor().equals("Negro")) {
                    if(estaMuerta(this.fichas[i])) {
                        contadorVictoria++;
                    }
                }
            }
        }else if (turno.equals("Negro")) {
            for (int i = 0; i < this.fichas.length; i++) {
                if (this.fichas[i].devuelveColor().equals("Blanco")) {
                    if(estaMuerta(this.fichas[i])) {
                        contadorVictoria++;
                    }
                }
            }
        }
        
        if (contadorVictoria == 12) {
            return true;
        }else{
            return false;
        }        
    }
    
    /**
     * Muestra todas las fichas del tablero
     *
     */
    public void mostrarFichas() {
        int x = 0;
        for (int i = 0; i < this.fichas.length; i++) {
            
            if (x % 4 == 0 && x != 0) {
                System.out.print("\n");
            }
            System.out.print("(");
            System.out.print(this.fichas[i].devuelvePosicion());
            System.out.print(")");
            System.out.print(this.fichas[i].devuelveColor());
            System.out.print("//");
            x++;
        }
        System.out.print("\n\n");
    }
    
    /**
     * guarda tablero en fichero
     *
     */
    public void guardar(PrintWriter pw) {
        String tablero = "";
        
        for (int i=0; i<fichas.length; i++) {
            tablero.concat(fichas[i].toString());
            tablero.concat("_");
        }
        
    }
    
    /**
     * equals
     *
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Ficha)) {
            return false;
        }
        Tablero tmp = (Tablero) obj;
        return (fichas.equals(tmp.fichas));
    }
    
    /**
     * toString
     *
     */
    @Override
    public String toString() {
        String tablero = "";
        
        for (int i=0; i<fichas.length; i++) {
            tablero = tablero.concat(fichas[i].toString());
            tablero = tablero.concat("_");
        }
        
        return tablero;
    }
}
