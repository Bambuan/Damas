/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.BevelBorder;
import modelo.Posicion;
import modelo.Ficha;

/**
 *
 * @author Guille
 */
public class CasillaVista extends JLabel{
    private boolean seleccionado;
    private Posicion posicion;
    private Ficha ficha;
    private static final String RUTA_RECURSOS = "/recursos/";
    public static final String ICONO_FICHA_BLANCA = "fichaBlancacursor.png";
    public static final String ICONO_FICHA_NEGRA = "fichaNegracursor.png";
    public static final String ICONO_FICHA_NEGRA_REINA = "fichaNegraReina.png";
    public static final String ICONO_FICHA_BLANCA_REINA = "fichaBlancaReina.png";
    
    CasillaVista(Ficha ficha, Posicion posicion, Icon icono) {
        this.seleccionado = false;
        this.ficha = ficha;
        this.posicion = posicion;
        setIcon(icono);
        setOpaque(true);
        setHorizontalAlignment(SwingConstants.CENTER);
        setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
        //setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
    }
  
    /**
     * Construye una vista sin icono y sin ficha de la casilla en una posición 
     * 
     */
    CasillaVista(Posicion posicion) {
      this(null, posicion, null);
    }
    /**
     * Construye una vista sin icono de la casilla en una posición 
     * 
     */
    CasillaVista(Ficha ficha,Posicion posicion) {
      this(ficha, posicion, null);
    }
    /**
     * Devuelve la posición de la casilla
     * 
     */
    Posicion devuelvePosicion() {
      return posicion;
    }
      /**
     * Devuelve la ficha de la casilla
     * 
     */
    Ficha devuelveFicha(){
        return ficha;
    }
 
    public void quitarFicha(){
        this.ficha = null;
        setIcon(null);
    }
    
    public void ponerFichaNegra(){
        setText(null);
        setIcon(new ImageIcon(this.getClass().getResource(RUTA_RECURSOS + ICONO_FICHA_NEGRA)));
    }  
    public void ponerFichaBlanca(){
        setText(null);
        setIcon(new ImageIcon(this.getClass().getResource(RUTA_RECURSOS + ICONO_FICHA_BLANCA)));
    }
    public void hacerReina(Ficha ficha){
        hacerReinaBlanca();
        if (ficha.devuelveColor().equals("Negro")){
            hacerReinaNegra();
        }
    }
    public void hacerReinaBlanca(){
        setIcon(new ImageIcon(this.getClass().getResource(RUTA_RECURSOS + ICONO_FICHA_BLANCA_REINA)));        
    }
    public void hacerReinaNegra(){
        setIcon(new ImageIcon(this.getClass().getResource(RUTA_RECURSOS + ICONO_FICHA_NEGRA_REINA)));
    }
    
    public void setFicha(Ficha fichaN){
        this.ficha = fichaN;
        if(ficha.devuelveColor().equals("Blanco")){
            ponerFichaBlanca();
        }
        else{
            ponerFichaNegra();
        }
    }

    public void marcarPosiblesMovimiento() {
        setForeground(Color.YELLOW);
    }
    void marcar() {
        this.seleccionado = true;
        setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
    }
    
    void desmarcar() {
        setFicha(null);
        setIcon(null);
    }
    /**
     * Sobreescribe toString
     * 
     */  
    @Override
    public String toString() {
      return posicion.toString();
    }
}
