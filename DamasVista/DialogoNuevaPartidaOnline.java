/**
 *
 * @author Guille
 */
package vista;

import control.OyenteVista;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * Dialogo para nueva partida online
 * 
 */
class DialogoNuevaPartidaOnline extends JDialog  {
  private static final int ANCHURA = 300;
  private static final int ALTURA_ETIQUETA = 14;
  
  private DamasVista vista;
//  private Localizacion localizacion;
  private String idJugador;
  private DefaultListModel<String> listaJugadoresOnline;
  private JButton botonAceptar;
  private Cursor cursor;

  /**
   * Construye diálogo nueva partida online
   * 
   */
  DialogoNuevaPartidaOnline(DamasVista vista,String idJugador) {      
    super(vista, false);
        
    this.vista = vista;
    this.idJugador = idJugador;
    
//    localizacion = Localizacion.devolverInstancia(vista, lenguaje, pais);
    this.setTitle("Seleccionar jugador contrario"/*localizacion.devuelve(
            Localizacion.TITULO_DIALOGO_NUEVA_ONLINE)*/);
    
    // Al cerrar eliminamos
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        cerrar();
      }
    });
   
    // cerramos con ESC
    ActionListener cancelListener = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        cerrar();
      }
    };
    KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    getRootPane().registerKeyboardAction(cancelListener, stroke, 
                                         JComponent.WHEN_IN_FOCUSED_WINDOW);

    this.setResizable(false);
    
    // creamos elementos
    setLayout(new BorderLayout());
    
    JLabel etiqueta = new JLabel("Jugadores online"/*localizacion.devuelve(
            Localizacion.ETIQUETA_JUGADORES_ONLINE)*/);
    etiqueta.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, ALTURA_ETIQUETA));
    add(etiqueta, BorderLayout.NORTH);

    listaJugadoresOnline = new DefaultListModel<>();
    listaJugadoresOnline.addElement("Esperando jugadores online"/*localizacion.devuelve(
            Localizacion.ETIQUETA_ESPERANDO_JUGADORES_ONLINE)*/);
    
    JList<String> jugadoresOnline = new JList<>(listaJugadoresOnline);
    jugadoresOnline.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    jugadoresOnline.addListSelectionListener(new ListSelectionListener() {
      public void valueChanged(ListSelectionEvent e) {
        // Habilitamos botón al seleccionar cualquier jugador  
        botonAceptar.setEnabled(true);     
      }
    });
    JScrollPane panelJugadores = new JScrollPane(jugadoresOnline);
    
    panelJugadores.setPreferredSize(
       new Dimension(ANCHURA, panelJugadores.getPreferredSize().height));
    
    add(panelJugadores, BorderLayout.CENTER);
    
    JPanel panelBotones = new JPanel();
    panelBotones.setLayout(new FlowLayout());
    botonAceptar = new JButton("Aceptar"/*localizacion.devuelve(localizacion.BOTON_ACEPTAR)*/);
    botonAceptar.setEnabled(false);
    botonAceptar.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
         //vista.notificacion(OyenteVista.Evento.SALIR, e, e);
         vista.notificacion(OyenteVista.Evento.INICIAR_PARTIDA_ONLINE, 
                            jugadoresOnline.getSelectedValue(),null);    
         cerrar();
      } 
    });
    panelBotones.add(botonAceptar);

    JButton botonCancelar = 
            new JButton("Cancelar"/*localizacion.devuelve(localizacion.BOTON_CANCELAR)*/);
    getRootPane().setDefaultButton(botonCancelar);    
    botonCancelar.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        cerrar();
      } 
    });
    panelBotones.add(botonCancelar);
    add(panelBotones, BorderLayout.SOUTH);
         
    pack();  // ajusta ventana y sus componentes
    setLocationRelativeTo(vista);  // centra en ventana principal    
    setVisible(true);    

    // ponemos cursor de espera mientras llega lista jugadores online
    cursor = getCursor();
    this.setCursor(new Cursor(Cursor.WAIT_CURSOR));    
    
    vista.notificacion(OyenteVista.Evento.PEDIR_JUGADORES_ONLINE, null,null);
  }

  /**
   * Cierra diálogo
   * 
   */  
  private void cerrar() {
    //vista.habilitarEventos(true);
    dispose();    
  }
 
  /**
   * Pone lista de jugadores online
   * 
   */
  void ponerListaJugadores(List<String> jugadores) {
    listaJugadoresOnline.removeAllElements();
    
    if (jugadores.size() == 0) {
      vista.mensajeDialogo("No hay jugadores online" /*localizacion.devuelve(
              Localizacion.MSG_NO_HAY_JUGADORES_ONLINE)*/);
      cerrar();
    } else {
      for (String jugador : jugadores) {
        listaJugadoresOnline.addElement(jugador);
      }
    }
    // ponemos cursor normal
    setCursor(cursor);
  }
}
