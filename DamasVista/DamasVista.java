 /**
 * DamasVista.java
 * Guillermo Bambó Pueyo
 * 
 */
package vista;
//import control.Damas;
import control.OyenteVista;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.GridLayout;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import modelo.Ficha;
import modelo.Partida;
import modelo.Tupla;
import modelo.Posicion;


public class DamasVista extends JFrame implements ActionListener, Observer{
    private static DamasVista instancia = null;
    
    private static final String RUTA_RECURSOS = "/recursos/";
    private static final String TITULO = "DAMAS";
    
    //MENUS
    private static final String MENU_FICHERO = "Fichero";
    private static final String MENU_ITEM_SALIR = "Salir";
    private static final char ATAJO_MENU_ITEM_SALIR = 'S';
    
    
    
    //TURNO Y BARRA HERRAMIENTAS
    private  String turno = "Blanco";
    JTextField turnoMenu;
    private JButton botonNueva;
    private JButton botonNuevaOnline;
    private JButton botonAbrir;
    private JButton botonGuardar;
    private JButton botonFinalizarOnline;
    
    //ICONOS BOTONES Y LOCALIZACION
    private static final String ICONO_NUEVA = "nueva.png";
    public static final String MENU_ITEM_NUEVA = "MENU_ITEM_NUEVA";
    private static final String ICONO_ABRIR = "abrir.png";
    public static final String MENU_ITEM_ABRIR = "MENU_ITEM_ABRIR";
    private static final String ICONO_GUARDAR = "guardar.png";
    public static final String MENU_ITEM_GUARDAR = "MENU_ITEM_GUARDAR";
    
    private String idJugador;
    private DialogoNuevaPartidaOnline dialogoNuevaPartidaOnline;
    private static final String ICONO_NUEVA_ONLINE = "nueva_online.png";
    private static final String MENU_ITEM_NUEVA_ONLINE = "MENU_ITEM_NUEVA_ONLINE";
    private static final String ICONO_FINALIZA_ONLINE = "finalizar.png";
    private static final String MENU_ITEM_FINALIZA_ONLINE = "MENU_ITEM_FINALIZA_ONLINE";
    
    //MENSAJES ERRORES 
    public static final String FICHA_OBLIGATORIO_COMER = "Tienes fichas para comer";
    public static final String POSICION_INVALIDA_PARA_MOVER = "No puedes mover ahí";
    public static final String FICHA_DEL_OPONENTE = "No es tu ficha";
    public static final String ERROR_LISTA_JUGADORES_ONLINE = "No hay jugadores online";
    public static final String ETIQUEDA_INVITACION_JUGAR = "Invitacion jugar";
    public static final String FINALIZAR_PARTIDA = "uiere finalizar partida";
    public static final String ERROR_COMUNICACION_REALIZAR_MOVIMIENTO = "Error comunicacion realizar movimiento.";
    public static final String ETIQUETA_ERROR_SOLICITUD_NUEVA_PARTIDA_ONLINE = "Error solicitud nueva partida online.";
    public static final String ETIQUETA_JUGADOR_NO_ACEPTA_JUGAR = " no acepta partida.";
    public static final String ERROR_GUARDAR_FICHERO_PUNTUACION = "fichero puntuacion no guardado.";
    public static final String MENSAJE_GUARDAR_PARTIDA = "Guardar partida";
    public static final String ERROR_GUARDAR_PARTIDA = "Error guardando la parida";
    public static final String ERROR_CARGAR_FICHERO_PUNTUACION = "Error cargar fichero puntuacion";
    public static final String PARTIDA_NO_LEIDA = "Partida no leida";
    public static final String FICHERO_PARTIDA_NO_ENCONTRADO = "fichero no encontrado";
    public static final String ETIQUETA_NUEVA_PARTIDA_ONLINE_1 = "Adversario: ";
    public static final String ETIQUETA_NUEVA_PARTIDA_ONLINE_2 = "Turno: ";
    public static final String HAY_GANADOR = "Ganador";
    
//    public static final String ICONO_FICHA_NEGRA_CURSOR = "imgRandom.png.jpg";
    public static final String CONFIRMACION_GUARDAR = "CONFIRMACION_GUARDAR";
    public static final String FILTRO_PARTIDAS = "FILTRO_PARTIDAS";
    public static final String EXT_FICHERO_PARTIDA = ".ter";
    public static final int ABRIR_FICHERO = 0;
    public static final int GUARDAR_FICHERO = 1;
    public static final int OPCION_SI = JOptionPane.YES_OPTION;
    
    private OyenteVista oyenteVista;
    private TableroVista tableroVista;

    private Ficha fichaTurno;
    private String turnoOnline;
    private Ficha[] fichas; // PROBAR A DECLARARLO DENTRO DEL CONSTRUCTOR en vez de this.partidaActual..
    private Partida partidaActual;
    
    private JPanel panelCentral;
    
    Toolkit TK = Toolkit.getDefaultToolkit();
    
    
    /**
     * Construye la vista del tablero de filas x columnas con el oyente para
     * eventos de la interfaz de usuario indicado
     *
     */
    private DamasVista(OyenteVista oyenteVista,int filas, int columnas, Partida partidaActual, String idJugador, Boolean invertido) {
        super();
        this.oyenteVista = oyenteVista;
        this.partidaActual = partidaActual;
        this.fichas = partidaActual.devuelveTablero().devuelveFichas();
        this.fichaTurno = null;
        this.turno = partidaActual.devuelveTurno();
        this.idJugador = idJugador;

         addWindowListener(new WindowAdapter() {
	      @Override
	      public void windowClosing(WindowEvent e) {
	        oyenteVista.eventoProducido(OyenteVista.Evento.SALIR,null, null);
	      }
	    });
        setLayout(new BorderLayout());
        setTitle(TITULO);
  
        setCursor(new Cursor(HAND_CURSOR)); //CURSOOOOOOOOOOOOOOOOOOOOR!!!!!!!!!!!!!!
  
        JPanel panelNorte = new JPanel();
	panelNorte.setLayout(new GridLayout(2,1));
	creaMenus(panelNorte);
	creaBarraHerramientas(panelNorte);
	add(panelNorte, BorderLayout.NORTH);
        
        panelCentral = new JPanel();
	panelCentral.setLayout(new BorderLayout());
        
        if (invertido != true){
        ponerTurnoOnline(null);
        creaTablero(panelCentral,filas,columnas);
        }
        else {
            creaTableroInvertido(panelCentral, filas, columnas, invertido);
        }
        add(panelCentral, BorderLayout.CENTER);      
        
        colocarPartida(partidaActual.devuelveTablero().devuelveFichas());

        
        pack();  // ajusta ventana y sus componentes
	setLocationRelativeTo(null);  // centra en la pantalla    

        setResizable(false);
        setVisible(true);
            
    }
    /**
    * Devuelve la instancia de la vista del tablero
    * 
    * @return instancia
    */        
    private void creaBarraHerramientas(JPanel panelNorte) {
        JToolBar barra = new JToolBar();
        barra.setFloatable(false);
        
        turnoMenu = new JTextField();
        turnoMenu.setHorizontalAlignment(SwingConstants.CENTER);
        turnoMenu.setText("Turno: " + turno);
        barra.add(turnoMenu);
                
        botonNueva = new JButton(new ImageIcon(
        this.getClass().getResource(RUTA_RECURSOS + ICONO_NUEVA)));
        botonNueva.setToolTipText(MENU_ITEM_NUEVA);
        botonNueva.addActionListener(this);
        botonNueva.setActionCommand(MENU_ITEM_NUEVA);
        barra.add(botonNueva);
        
        botonAbrir = new JButton(new ImageIcon(
           this.getClass().getResource(RUTA_RECURSOS + ICONO_ABRIR)));    
        botonAbrir.setToolTipText(MENU_ITEM_ABRIR);
        botonAbrir.addActionListener(this);
        botonAbrir.setActionCommand(MENU_ITEM_ABRIR);
        barra.add(botonAbrir);

        botonGuardar = new JButton(new ImageIcon(
           this.getClass().getResource(RUTA_RECURSOS + ICONO_GUARDAR)));    
        botonGuardar.setEnabled(false);
        botonGuardar.setToolTipText(MENU_ITEM_GUARDAR);
        botonGuardar.addActionListener(this);
        botonGuardar.setActionCommand(MENU_ITEM_GUARDAR);
        barra.add(botonGuardar);
        barra.addSeparator();
        
        botonNuevaOnline = new JButton(new ImageIcon(
            this.getClass().getResource(RUTA_RECURSOS + ICONO_NUEVA_ONLINE)));
        botonNuevaOnline.setToolTipText(MENU_ITEM_NUEVA_ONLINE);
        botonNuevaOnline.setEnabled(false);
        botonNuevaOnline.addActionListener(this);
        botonNuevaOnline.setActionCommand(MENU_ITEM_NUEVA_ONLINE);
        barra.add(botonNuevaOnline);
        
        botonFinalizarOnline = new JButton(new ImageIcon(
            this.getClass().getResource(RUTA_RECURSOS + ICONO_FINALIZA_ONLINE)));
        botonFinalizarOnline.setToolTipText(MENU_ITEM_FINALIZA_ONLINE);
        botonFinalizarOnline.setEnabled(false);
        botonFinalizarOnline.addActionListener(this);
        botonFinalizarOnline.setActionCommand(MENU_ITEM_FINALIZA_ONLINE);
        barra.add(botonFinalizarOnline);
        
        
        panelNorte.add(barra);
        
        
    }
   /**
    * Crea el menu desplegable de la ventana
    * 
    * 
    */ 
    private void creaMenus(JPanel panelNorte) {
        JMenu menuFichero = new JMenu(MENU_FICHERO);
        menuFichero.addSeparator();
                        
        JMenuItem menuFicheroSalir = 
                new JMenuItem(MENU_ITEM_SALIR, ATAJO_MENU_ITEM_SALIR);
        menuFicheroSalir.addActionListener(this);
        menuFicheroSalir.setActionCommand(MENU_ITEM_SALIR);
        menuFichero.add(menuFicheroSalir);    

        JMenuBar menuPrincipal = new JMenuBar();
        menuPrincipal.add(menuFichero);

        panelNorte.add(menuPrincipal);		    
    }
    
    /**
    * Crea la vista de un tablero
    * 
    * 
    */ 
    private void creaTablero(JPanel panel,int filas, int columnas) {
        tableroVista = new TableroVista(this, filas, columnas,
                TableroVista.RECIBIR_EVENTOS_RATON);
        panel.add(tableroVista,BorderLayout.CENTER);
    }
    
    /**
    * Crea la vista de un tablero invertido (negras abajo)
    * 
    * 
    */
    private void creaTableroInvertido(JPanel panel, int filas,int columnas, Boolean invertido){
        tableroVista = new TableroVista(this,filas,columnas,TableroVista.RECIBIR_EVENTOS_RATON,invertido);
        panel.add(tableroVista, BorderLayout.CENTER);
    }
    
    /**
    * Invierte el tablero de la instancia 
    * 
    * 
    */
    public void invertirTablero(){
        panelCentral.removeAll();
        tableroVista = new TableroVista(this, 8, 8, TableroVista.RECIBIR_EVENTOS_RATON, true);
        panelCentral.add(tableroVista, BorderLayout.CENTER);
        ponerFichas(partidaActual.devuelveTablero().devuelveFichas());
    }
    
    /**
    * Pone titulo a la ventana
    * 
    * 
    */
    public void ponerTitulo(String nombreFichero) {
        StringBuilder titulo = new StringBuilder(TITULO);          
        if (! nombreFichero.equals("")) {
          titulo.insert(0, nombreFichero + " en juego. ");
        }     
        this.setTitle(titulo.toString());
    }
    
    /**
    * Sobreescribo el actionPerformed para recibir eventos de los botones y menus
    * 
    * 
    */        
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()) {
            case MENU_ITEM_NUEVA:
                oyenteVista.eventoProducido(OyenteVista.Evento.INICIAR_PARTIDA_LOCAL, null, null);
            break;
            case MENU_ITEM_NUEVA_ONLINE:
                dialogoNuevaPartidaOnline = new DialogoNuevaPartidaOnline(this,idJugador); 
            break;
            case MENU_ITEM_FINALIZA_ONLINE:
                oyenteVista.eventoProducido(OyenteVista.Evento.ACABAR_PARTIDA, null, null);
            break;    
            case MENU_ITEM_ABRIR:
                oyenteVista.eventoProducido(OyenteVista.Evento.ABRIR_PARTIDA, null, null);
            break;
            case MENU_ITEM_GUARDAR:
                oyenteVista.eventoProducido(OyenteVista.Evento.GUARDAR_PARTIDA, null, null);
            break;
            case MENU_ITEM_SALIR:
                oyenteVista.eventoProducido(OyenteVista.Evento.SALIR,null,null);
            break;
        }	
    }
    
    /**
    * Realizamos una notificacion de evento producido
    * 
    * 
    */
    public void notificacion(OyenteVista.Evento evento,Object obj, Object obj2) {
        oyenteVista.eventoProducido(evento,obj,obj2);    
    }
    
    /**
    * Cambia de turno la vista
    * 
    * 
    */
    public void siguienteTurno() {
        if (turno.equals("Blanco")) {
            turno = "Negro";
            turnoMenu.setText("Turno: " + turno);
        } else {
            turno = "Blanco";
            turnoMenu.setText("Turno: " + turno);
        }
    }
    
    /**
    * Devuelve el turno de la partida
    * 
    * 
    */
    public String devuelveTurno(){
        return turno;
    }
    
    /**
    * Devuelve el turno del jugador online
    * 
    * 
    */
    public String devuelveTurnoOnline(){
        return turnoOnline;
    }
    
    /**
    * Metodo habilitador de eventos, activa o desactiva los botones
    * 
    * 
    */
    public void habilitarEvento(OyenteVista.Evento evento, boolean habilitacion) {
        switch (evento) {
            case INICIAR_PARTIDA_ONLINE:
                botonNuevaOnline.setEnabled(habilitacion);
            break;
     
            case GUARDAR_PARTIDA:
                botonGuardar.setEnabled(habilitacion);              
            break;
            
            case ACABAR_PARTIDA:
                botonFinalizarOnline.setEnabled(habilitacion);
            break;
        }
    }
    /**
    * Devolvemos la instancia de la partida
    * 
    * 
    */
    public static synchronized DamasVista devolverInstancia(OyenteVista oyenteIU,int filas,int columnas, Partida partidaActual, String idJugador, Boolean invertido) {
	if (instancia == null){
            instancia = new DamasVista(oyenteIU, filas, columnas, partidaActual, idJugador, invertido);
        }
        return instancia;
    } 
    /**
    * set de la ficha seleccionada para mover 
    * 
    * 
    */    
    public void ponerFichaMover(Ficha ficha){
        fichaTurno = ficha;  
    }
    /**
    * Coloca las fichas en el tablero
    * 
    * 
    */
    public void ponerFichas(Ficha[] fichas){
        tableroVista.ponerFichas(fichas);
    }   
    /**
    * Devuelve la ficha seleccionada para mover 
    * 
    * 
    */
    public Ficha devuelveFichaTurno(){
        return fichaTurno;
    }
    /**
    * Cambia el turno online de la partida
    * 
    * 
    */
    public void ponerTurnoOnline(String turno){
        this.turnoOnline = turno;
    }
    
    /**
    * Ventana para seleccionar fichero, depende de operacion
    *  operacion =GUARDAR_FICHERO o operacion = ABRIR_FICHERO
    * 
    */
    public String seleccionarFichero(int operacion) {
        String nombreFichero = null;
        int resultado = 0;
    
        JFileChooser dialogoSeleccionar = new JFileChooser(new File("."));
        FileNameExtensionFilter filtro = 
            new FileNameExtensionFilter(FILTRO_PARTIDAS, 
                    EXT_FICHERO_PARTIDA.substring(1));
    
        dialogoSeleccionar.setFileFilter(filtro);

        if (operacion == ABRIR_FICHERO) {
          resultado = dialogoSeleccionar.showOpenDialog(this);
        } else {
          resultado = dialogoSeleccionar.showSaveDialog(this);
        }

        if(resultado == JFileChooser.APPROVE_OPTION) {
          nombreFichero = dialogoSeleccionar.getSelectedFile().getName();
          // pone extensión si hace falta al guardar
          if (! nombreFichero.endsWith(EXT_FICHERO_PARTIDA) && 
              (operacion == GUARDAR_FICHERO)) {
            nombreFichero = nombreFichero + EXT_FICHERO_PARTIDA;
          }
    }
    
    return nombreFichero;
  }
    
    /**
     * Escribe mensaje con diÃ¡logo modal
     *
     */
    public void mensajeDialogo(String mensaje) {
        JOptionPane.showMessageDialog(this, mensaje, null,
                JOptionPane.INFORMATION_MESSAGE, null);
    }
    
    
   
    
   /**
   * Cuadro diálogo de confirmación acción
   * 
   */    
    public int mensajeConfirmacion(String mensaje) {
        return JOptionPane.showConfirmDialog(this, mensaje, 
            TITULO + " " + null,
            JOptionPane.YES_NO_OPTION, 
            JOptionPane.QUESTION_MESSAGE); 
    } 
    /**
    * Limpia todo el tablero
    * 
    */ 
    public void limpiarTablero(){
        tableroVista.limpiarTablero();
    }
    /**
     * Pone lista de jugadores online
     * 
     */
    public void ponerListaJugadores(java.util.List<String> jugadores) { 
      // lista puede llegar después de haber sido cancelado   
      if (dialogoNuevaPartidaOnline != null) { 
        dialogoNuevaPartidaOnline.ponerListaJugadores(jugadores);
      } 
    }

    /**
     * Sobreescribimos el update
     * 
     */
    @Override
    public void update(Observable o, Object arg) {
        Tupla tupla = (Tupla) arg;
        if (tupla.a.equals(OyenteVista.Evento.CAMBIA_TURNO)) {
            cambiarTurno((String) tupla.b);
        }
        else if (tupla.a.equals(OyenteVista.Evento.ABRIR_PARTIDA)){
            colocarPartida((Ficha[])tupla.b);
        }
        else if(tupla.a.equals(OyenteVista.Evento.HACER_REINA)){
            hacerReina((Ficha)tupla.b);
        }
        else if(tupla.a.equals(OyenteVista.Evento.REALIZAR_MOVIMIENTO)){
            ponerFichas((Ficha[])tupla.b);
        }
        else if (tupla.a.equals(OyenteVista.Evento.RECOLOCAR_FICHA)){
            ponerFichas((Ficha[])tupla.b);
        }
        else if (tupla.a.equals(OyenteVista.Evento.HAY_VICTORIA)){
            ganador((String)tupla.b);
        }
    }
    /**
     * Pone las fichas en el tablero
     * 
     */
    public void colocarPartida(Ficha[] fichas){
        tableroVista.ponerFichas(fichas);
    }
    /**
     * Cambia el turno en la vista
     * 
     */
    public void cambiarTurno(String turnoNuevo) {
        turno = turnoNuevo;
        turnoMenu.setText("Turno: " + turnoNuevo);
    }
    /**
     * Muestra el ganador
     * 
     */
    public void ganador(String turno){
        mensajeDialogo("El ganador es: "+ turno);
    }
    /**
     * Elimina el icono de la ficha
     * 
     */
    public void comerFicha(Posicion posicion){
        tableroVista.comerFicha(posicion);
    }
    /**
     * Pone el icono de reina
     * 
     */    
    public void hacerReina(Ficha ficha){
        tableroVista.hacerReina(ficha);
    }

}
