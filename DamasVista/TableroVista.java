 /**
 * TableroVista.java
 * Guillermo Bambó Pueyo
 * 
 */
package vista;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import javax.swing.JPanel;
import control.OyenteVista;
import control.OyenteVista.Evento;
import java.awt.Color;
import java.awt.event.MouseEvent;
import modelo.Posicion;
import modelo.Tablero;
import modelo.Ficha;


public class TableroVista extends JPanel{
    public static final boolean RECIBIR_EVENTOS_RATON = true;
    public static final boolean NO_RECIBIR_EVENTOS_RATON = false;
    
    public static final String ICONO_FICHA_BLANCA = "fichaBlancacursor.png";
    public static final String ICONO_FICHA_NEGRA = "fichaNegracursor.png";
    
    private static final int ALTURA_FILA = 70;
    private static final int ANCHURA_COLUMNA = 70;
    public Tablero tablero;
    private DamasVista damasVista;    
    CasillaVista[][] casillas;
    CasillaVista casillaVista;

    /**
    * Constructor de un tablero normal
    * 
    * 
    */    
    public TableroVista(DamasVista vista, int filas, int columnas, boolean recibe_eventos_raton) {
        this.setEnabled(false);
        this.damasVista = vista;
        setLayout(new GridLayout(filas, columnas));
        casillas = new CasillaVista[filas][columnas];
        String turnoOnline = damasVista.devuelveTurnoOnline();

        for(int fil = 7; fil >=  0; fil--) 
          for(int col = 0; col < columnas; col++) {
            casillas[fil][col] = new CasillaVista(null,new Posicion(fil, col),null);         
            add(casillas[fil][col]); 

            if ((fil + col) % 2 == 0){ 
                casillas[fil][col].setBackground(Color.red); //Colorea los cuadros rojoh para dibujar el tablero   
            }

            if (recibe_eventos_raton) {
                casillas[fil][col].addMouseListener(new MouseAdapter() { 
                    @Override
                    public void mousePressed(MouseEvent e) { 
                        if (casillaVista != null) {
                            casillaVista.desmarcar();
                        }

                        CasillaVista casilla = (CasillaVista)e.getSource();
                        Ficha ficha = casilla.devuelveFicha(); 

                        if(ficha==null && damasVista.devuelveFichaTurno() == null) {
//                            vista.mensajeDialogo("Selecciona una ficha primero");
                        }
                        else if(ficha != null && damasVista.devuelveFichaTurno() == null){
                            //CONTROLAR FICHAS DE OPONENTE
                            if(ficha.devuelveColor() == (damasVista.devuelveTurno()) &&
                                    turnoOnline == null){
                                casilla.quitarFicha();    
                                damasVista.ponerFichaMover(ficha);
                                
                            } else if ( ficha.devuelveColor() != (damasVista.devuelveTurno()) &&
                                    ficha.devuelveColor() != turnoOnline){
                                //vista.mensajeDialogo("No es tu ficha");
                            }
                        }
                        //CONTROLAR EL MOVIMIENTO
                        else if (ficha == null && damasVista.devuelveFichaTurno() != null) {
                            if(damasVista.devuelveTurno().equals("Blanco") ){
                                if(dentroTablero(casilla.devuelvePosicion())){
                                    vista.notificacion(OyenteVista.Evento.REALIZAR_MOVIMIENTO,
                                            damasVista.devuelveFichaTurno(),casilla.devuelvePosicion());
                                    vista.habilitarEvento(Evento.GUARDAR_PARTIDA, true);
                                    damasVista.ponerFichaMover(null);
                                }                             
                            }
                            else{
                                if(dentroTablero(casilla.devuelvePosicion())){
                                    vista.notificacion(OyenteVista.Evento.REALIZAR_MOVIMIENTO,
                                            damasVista.devuelveFichaTurno(),casilla.devuelvePosicion());
                                    vista.habilitarEvento(Evento.GUARDAR_PARTIDA, true);
                                    damasVista.ponerFichaMover(null);
                                }
                            }                        
                        }
                        //CONTROLAR SELECCION DE SOLAMENTE UNA PIEZA
                        else if (ficha != null && damasVista.devuelveFichaTurno() != null){
                            if(damasVista.devuelveFichaTurno() == null){
                                    casilla.quitarFicha();
                                    damasVista.ponerFichaMover(ficha);
                            }
                            else{
                                vista.mensajeDialogo("Ficha ya seleccionada");
                            }
                        }
                    }
                });
            } 

        }
    
    this.setPreferredSize(new Dimension(filas * ALTURA_FILA, 
                                        columnas * ANCHURA_COLUMNA));
    this.setOpaque(false);
    }

    /**
    * Constructor de un tablero invertido
    * 
    * 
    */
    public TableroVista(DamasVista vista, int filas, int columnas, boolean recibe_eventos_raton, boolean invertido) {
        this.setEnabled(false);
        this.damasVista = vista;
        String turnoOnline = "Negro";

        setLayout(new GridLayout(filas, columnas));
        casillas = new CasillaVista[filas][columnas];

        for(int fil = 0; fil < 8; fil++) 
          for(int col = 7; col >= 0; col--) {
            casillas[fil][col] = new CasillaVista(null,new Posicion(fil, col),null);         
            add(casillas[fil][col]); 

            if ((fil + col) % 2 == 0){ 
                casillas[fil][col].setBackground(Color.red); //Colorea los cuadros rojoh para dibujar el tablero   
            }
            if (recibe_eventos_raton) {
                casillas[fil][col].addMouseListener(new MouseAdapter() { 
                    @Override
                    public void mousePressed(MouseEvent e) { 
                        if (casillaVista != null) {
                            casillaVista.desmarcar();
                        }

                        CasillaVista casilla = (CasillaVista)e.getSource();
                        Ficha ficha = casilla.devuelveFicha(); 

                        if(ficha==null && damasVista.devuelveFichaTurno() == null) {
                            //vista.mensajeDialogo("Selecciona una ficha primero");
                        }
                        else if(ficha != null && damasVista.devuelveFichaTurno() == null){
                            //CONTROLAR FICHAS DE OPONENTE
                            if(ficha.devuelveColor() == (damasVista.devuelveTurno()) &&
                                    turnoOnline == ficha.devuelveColor() ){
                                System.out.println(turnoOnline);
                                casilla.quitarFicha();    
                                damasVista.ponerFichaMover(ficha);
                                
                            } else if ( ficha.devuelveColor() != (damasVista.devuelveTurno()) &&
                                    ficha.devuelveColor() != turnoOnline){
                                //vista.mensajeDialogo("No es tu ficha");
                            }
                        }
                        //CONTROLAR EL MOVIMIENTO
                        else if (ficha == null && damasVista.devuelveFichaTurno() != null) {
                            if(damasVista.devuelveTurno().equals("Blanco") ){
                                if(dentroTablero(casilla.devuelvePosicion())){
                                vista.notificacion(OyenteVista.Evento.REALIZAR_MOVIMIENTO,
                                        damasVista.devuelveFichaTurno(),casilla.devuelvePosicion());
                                dentroTablero(casilla.devuelvePosicion());
                                vista.habilitarEvento(Evento.GUARDAR_PARTIDA, true);
                                damasVista.ponerFichaMover(null);
                                }
                            }
                            else{
                                if(dentroTablero(casilla.devuelvePosicion())){
                                vista.notificacion(OyenteVista.Evento.REALIZAR_MOVIMIENTO,
                                        damasVista.devuelveFichaTurno(),casilla.devuelvePosicion());
                                vista.habilitarEvento(Evento.GUARDAR_PARTIDA, true);
                                damasVista.ponerFichaMover(null);
                                }
                                
                            }                        
                        }
                        //CONTROLAR SELECCION DE SOLAMENTE UNA PIEZA
                        else if (ficha != null && damasVista.devuelveFichaTurno() != null){
                            if(damasVista.devuelveFichaTurno() == null){
                                    casilla.quitarFicha();
                                    damasVista.ponerFichaMover(ficha);
                            }
                            else{
                                //vista.mensajeDialogo("Ficha ya seleccionada");
                            }
                        }
                    }
                });
            }       
        }
        this.setPreferredSize(new Dimension(filas * ALTURA_FILA, 
                                            columnas * ANCHURA_COLUMNA));
        this.setOpaque(false);
    }

    /**
    * Coloca las fichas en el tablero
    * 
    * 
    */
    public void ponerFichas(Ficha[] fichas){
        limpiarTablero();
        for(int i = 0; i <= 23;i++){
            Posicion posicion = fichas[i].devuelvePosicion();
            ponerIconoCasilla(posicion, fichas[i]);
        }
    }
    
    /**
    * Pone el icono de la ficha en la casilla
    * 
    * 
    */
    void ponerIconoCasilla(Posicion posicion,Ficha ficha) { 
        if(ficha.devuelveColor().equals("Blanco")){
            if(ficha.estaMuerta() != true){                         
            casillas[posicion.devuelveFila()]
                    [posicion.devuelveColumna()].setFicha(ficha);
            }
            if(ficha.esReina() == true){
                hacerReina(ficha);
            }
        }
        else{
            if(ficha.estaMuerta()!=true){
                       
            casillas[posicion.devuelveFila()]
                    [posicion.devuelveColumna()].setFicha(ficha);
            }
            if(ficha.esReina() == true){
                hacerReina(ficha);
            }
        }
    }  
    /**
     * Comprueba la posicion, true si es la roja
     *
     * @param posicion
     * @return 
     */
    public boolean dentroTablero(Posicion posicion) {
        if ((posicion.devuelveColumna() + posicion.devuelveFila()) %2 == 0){
            return true;
        }
        return false;
    }
    
    /**
    * Hace reina una ficha
    * 
    * 
    */
    public void hacerReina(Ficha ficha){
        if(ficha.devuelvePosicion().devuelveColumna() == -10 
            && ficha.devuelvePosicion().devuelveFila()==-10){
            //no hace nada si la reina esta comida
        }
        else{
        casillas[ficha.devuelvePosicion().devuelveFila()]
                [ficha.devuelvePosicion().devuelveColumna()].hacerReina(ficha);
        }
    }    
    
    /**
    * Habilita o deshabilita el panel
    * 
    * 
    */
    public void habilitar(boolean habilitacion) {
             this.setEnabled(habilitacion);
    }
    
    /**
    * Limpia el tablero
    * 
    * 
    */
    public void limpiarTablero(){
        for(int fil=0; fil < 8; fil++){
            for(int col = 0; col < 8; col++){
                casillas[fil][col].quitarFicha();
            }
        }
    }
    
    /**
    * Elimina una ficha del panel
    * 
    * 
    */
    public void comerFicha(Posicion posicion){
        casillas[posicion.devuelveFila()][posicion.devuelveColumna()].quitarFicha();
    }   

}